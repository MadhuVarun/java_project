package com.hcl.employeeservice.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

                                              //Main Class

@SpringBootApplication
@EnableEurekaClient
@ComponentScan(basePackages = "com.hcl.employeeservice.main")
@EntityScan(basePackages = "com.hcl.employeeservice.main.entity")
@EnableJpaRepositories(basePackages = "com.hcl.employeeservice.main.repository")
public class EmployeeserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeserviceApplication.class, args);
		System.out.println("employee-service started");
	}

}
