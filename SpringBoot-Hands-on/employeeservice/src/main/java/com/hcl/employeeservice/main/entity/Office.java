package com.hcl.employeeservice.main.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Office {
	@Id
	@Column
//	@GeneratedValue(strategy = GenerationType.AUTO)
	private int officeCode;
	@Column
	private String city;
	@Column
	private long phone;
	@Column
	private String adressLine1;
	@Column
	private String adressLine2;
	@Column
	private String state;
	@Column
	private String country;
	@Column
	private int postalCode;
	@Column
	private String teritory;

	public Office() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Office(int officeCode, String city, long phone, String adressLine1, String adressLine2, String state,
			String country, int postalCode, String teritory) {
		super();
		this.officeCode = officeCode;
		this.city = city;
		this.phone = phone;
		this.adressLine1 = adressLine1;
		this.adressLine2 = adressLine2;
		this.state = state;
		this.country = country;
		this.postalCode = postalCode;
		this.teritory = teritory;
	}

	public int getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(int officeCode) {
		this.officeCode = officeCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public String getAdressLine1() {
		return adressLine1;
	}

	public void setAdressLine1(String adressLine1) {
		this.adressLine1 = adressLine1;
	}

	public String getAdressLine2() {
		return adressLine2;
	}

	public void setAdressLine2(String adressLine2) {
		this.adressLine2 = adressLine2;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}

	public String getTeritory() {
		return teritory;
	}

	public void setTeritory(String teritory) {
		this.teritory = teritory;
	}

	@Override
	public String toString() {
		return "Office [officeCode=" + officeCode + ", city=" + city + ", phone=" + phone + ", adressLine1="
				+ adressLine1 + ", adressLine2=" + adressLine2 + ", state=" + state + ", country=" + country
				+ ", postalCode=" + postalCode + ", teritory=" + teritory + "]";
	}

}