package com.hcl.employeeservice.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

import com.hcl.employeeservice.main.entity.Office;



/// Data Access Layer
//    Manage the records or data in sql or in repository 


@Repository
public interface OfficeRepository  extends JpaRepository<Office, Integer>{

}
