package com.hcl.employeeservice.main.restcontroller;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.hcl.employeeservice.main.entity.Employee;
import com.hcl.employeeservice.main.entity.Office;
import com.hcl.employeeservice.main.repository.EmployeeRepository;
import com.hcl.employeeservice.main.repository.OfficeRepository;

///Business Logic  & Presentation Layer 

///CRUD Operation
@RestController
@RequestMapping(value = "/api")
public class EmployeeRestController {
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	OfficeRepository officeRepository;

	/// Posting,Creating
	@RequestMapping(value = "/office", method = RequestMethod.POST)
	public ResponseEntity<Object> addofficeDetails(@RequestBody Office office) {

		try {
			Office officeentityresult = officeRepository.save(new Office(office.getOfficeCode(), office.getCity(),
					office.getPhone(), office.getAdressLine1(), office.getAdressLine2(), office.getState(),
					office.getCountry(), office.getPostalCode(), office.getTeritory()));
			return new ResponseEntity<Object>(officeentityresult, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//// Posting,Creating
	@RequestMapping(value = "/employee", method = RequestMethod.POST)
	public ResponseEntity<Object> addEmployeDetails(@RequestBody Employee employee) {

		try {
			Employee employeeentityresult = employeeRepository.save(new Employee(employee.getEmployeeNumber(),
					employee.getLastName(), employee.getFristName(), employee.getExtension(), employee.getEmail(),
					employee.getReportTo(), employee.getJobTitle(), employee.getOffice()));

			return new ResponseEntity<Object>(employeeentityresult, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/// Getting,Fetching
	@RequestMapping(value = "/employee", method = RequestMethod.GET)
	public ResponseEntity<Object> getEmployeDetails() {
		List list = employeeRepository.findAll();

		return new ResponseEntity<Object>(list, HttpStatus.OK);
	}

	/// Getting,Fetching
	@RequestMapping(value = "/office", method = RequestMethod.GET)
	public ResponseEntity<Object> findoffices() {
		List list = officeRepository.findAll();

		return new ResponseEntity<Object>(list, HttpStatus.OK);
	}

	/// Getting,Fetching
	@RequestMapping(value = "/findemployees/{employeeNumber}", method = RequestMethod.GET)
	public ResponseEntity<Object> findEmployee(@PathVariable("employeeNumber") int employeeNumber) {

		try {
			Optional<Employee> result = employeeRepository.findById(employeeNumber);
			if (result.isPresent()) {
				return new ResponseEntity<Object>(result, HttpStatus.OK);
			} else {
				return new ResponseEntity<Object>("result not availble", HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/// Posting,Creating
	@RequestMapping(value = "/employee/{employeeNumber}", method = RequestMethod.POST)
	public ResponseEntity<Object> addEmployOffice(@PathVariable("employeeNumber") int employeeNumber,
			@RequestBody Office office) {
		try {
			Optional<Employee> result = employeeRepository.findById(employeeNumber);
			if (result.isPresent()) {
				Office officeentityresult = officeRepository.save(new Office(office.getOfficeCode(), office.getCity(),
						office.getPhone(), office.getAdressLine1(), office.getAdressLine2(), office.getState(),
						office.getCountry(), office.getPostalCode(), office.getTeritory()));
				return new ResponseEntity<Object>(officeentityresult, HttpStatus.CREATED);

			} else {
				return new ResponseEntity<Object>("result not availble", HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/// Getting,Fetching
	@RequestMapping(value = "/findemployeoffice/{employeeNumber}", method = RequestMethod.GET)
	public ResponseEntity<Object> findEmployeeoffices(@PathVariable("employeeNumber") int employeeNumber) {

		try {
			Optional<Employee> result = employeeRepository.findById(employeeNumber);
			if (result.isPresent()) {

				return new ResponseEntity<Object>(result, HttpStatus.OK);
			} else {
				return new ResponseEntity<Object>("result not availble", HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PutMapping(value = "/updateemployee")
	public ResponseEntity<Object> updateEmployee(@RequestBody Employee employee) {
		Employee result = employeeRepository.save(employee);

		return new ResponseEntity<Object>(result, HttpStatus.OK);
	}

	@PutMapping(value = "/updateoffice")
	public ResponseEntity<Object> updateOffice(@RequestBody Office office) {
		Office off = officeRepository.save(office);

		return new ResponseEntity<Object>(off, HttpStatus.OK);
	}

	@DeleteMapping(value = "/delete/{employeeNumber}")
	public ResponseEntity<Object> deletedById(@PathVariable("employeeNumber") int employeeNumber) {
		employeeRepository.deleteById(employeeNumber);
		return new ResponseEntity<Object>(" Deleted employeeNumber " + employeeNumber, HttpStatus.OK);
	}

}