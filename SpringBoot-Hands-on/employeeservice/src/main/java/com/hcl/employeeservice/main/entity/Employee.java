package com.hcl.employeeservice.main.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table
                    //Data access Layer
public class Employee    {
	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column
	private int employeeNumber;
	@Column
	private String lastName;
	@Column
	private String fristName;
	@Column
	private String extension;
	@Column
	private String email;
	@Column
	private String reportTo;
	@Column
	private String jobTitle;
	@ManyToOne
	private Office office;

	public Employee() {
	}

	

	
	public Employee(int employeeNumber, String lastName, String fristName, String extension, String email,
			String reportTo, String jobTitle, Office office) {
		super();
		this.employeeNumber = employeeNumber;
		this.lastName = lastName;
		this.fristName = fristName;
		this.extension = extension;
		this.email = email;
		this.reportTo = reportTo;
		this.jobTitle = jobTitle;
		this.office = office;
	}




	


	public int getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFristName() {
		return fristName;
	}

	public void setFristName(String fristName) {
		this.fristName = fristName;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getReportTo() {
		return reportTo;
	}

	public void setReportTo(String reportTo) {
		this.reportTo = reportTo;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}




	public Office getOffice() {
		return office;
	}




	public void setOffice(Office office) {
		this.office = office;
	}

	



	
}

	